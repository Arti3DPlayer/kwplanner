import os

ROOT_PATH = os.path.dirname(os.path.realpath(__file__))

# Each object should contain email, password, client_customer_id field
# To activate account you should run script with parameter:
# --new_account [index] Index starting from 0
ADWORDS_ACCOUNTS = [
    {
        'email': 'Info@sywebs.nl',
        'password': 'SMWtfv4y',
        'client_customer_id': '504-678-4290'
    }
]

# Advanced

# Number of parallel processes for fetching words
NUMBER_OF_PROCESSES = 2

KW_PLANNER_CHUNK_SIZE = 700

GS_USER_AGENT = 'Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.0.1) Gecko/2008071615 Fedora/3.0.1-1.fc9 Firefox/3.0.1'