## Getting Started

### Prerequisites

To run the script you need to install:

- [Docker](https://www.docker.com/products/docker-desktop) version 18.03.1 or higer
- [Python](https://www.python.org/downloads/release/python-365/) version 3.6.5 

### Creating the environment

Download project, open terminal and go to project folder:

```bash
cd /path/to/project/
```

Do command:

```bash
python3 -m venv env
```

Then:

```bash
cd env
source bin/activate
```

Go back to root folder:

```bash
cd ..
```

And do:

```bash
pip install -r requirements.txt
```

###  How to use

To run the program, do:

```bash
docker-compose up -d
```

To add new account you should login to account from new pc and enter confirmation sms. Run script:

```bash
ython register_account --email Info@sywebs.nl --password SMWtfv4y --client-customer-id 504-678-4290
```

You need to do previous command only one time if you login from new ip.

And then run parser:

```bash
python3 kw_planner.py --keywords-file-path /path/to/file/with/keywords --output-path /tmp/output.csv
```

Example:

```bash
python kw_planner.py --keywords-file-path data/test3.txt --output-path tmp/output.csv 
```

Program apply the following arguments:

`--keywords-file-path` - path to your txt file

`--output-path` - path to result `.csv` file


After finish do:

```bash
docker-compose stop
```

### Google ratings

To use google ratings make sure you inside virtal enviroment

Then:

```bash
cd env
source bin/activate
```

Go back to root folder:

```bash
cd ..
```

Then do:

```bash
python google_search_ratings.py --keywords-file-path data/example_kw_planner2.txt --output-path tmp/output2.csv
```

Where:

`--keywords-file-path` - path to file with keywords

`--output-path` - path to output `.csv` file