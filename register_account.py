import os
import time
import pickle
import random
import argparse
import shutil
import logging
import logging.config
import glob
import csv
import operator

import settings

from datetime import datetime
from urllib.parse import urlencode
import multiprocessing

from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities

from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait, Select
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException, NoSuchElementException,\
    WebDriverException, UnexpectedAlertPresentException, StaleElementReferenceException

from modules.workflow_handler import WorkflowHandler, WorkflowStatus, RuleSet, Rule, Priority,\
    PlainText, CaseLess, Mandatory, Url, Regex, document_handler, WorkflowError

from watchdog import Watchdog, WatchdogException
from utils import chunks, yes_or_no

LOGS_DIR = "logs"
DOWNLOADS_DIR = "downloads"
TEMPORARY_DIR = "tmp"

if not os.path.exists(os.path.join(settings.ROOT_PATH, LOGS_DIR)):
    os.makedirs(os.path.join(settings.ROOT_PATH, LOGS_DIR))

if not os.path.exists(os.path.join(settings.ROOT_PATH, DOWNLOADS_DIR)):
    os.makedirs(os.path.join(settings.ROOT_PATH, DOWNLOADS_DIR))

if not os.path.exists(os.path.join(settings.ROOT_PATH, TEMPORARY_DIR)):
    os.makedirs(os.path.join(settings.ROOT_PATH, TEMPORARY_DIR))

logging.config.fileConfig('logging.conf')
logger = logging.getLogger(__name__)


class KWPlannerWorkflowHandler(WorkflowHandler):

    @document_handler(Priority.Highest,
                      [RuleSet(Rule('ads.google.com/intl/en/home/', PlainText, CaseLess, Mandatory, Url))],
                      invocation_limit=1)
    def home_page(self):
        logger.debug("Home page")
        logger.debug(self.browser.current_url)
        login_link = self.__wait_element(xpath_selector='.//a[@class="cta-signin"]')
        self.__click_on_element(elem=login_link)

    @document_handler(Priority.Highest,
                      [RuleSet(Rule('accounts.google.com/signin/v2/identifier', PlainText, CaseLess, Mandatory, Url))],
                      invocation_limit=1)
    def login_email_page(self):
        logger.debug("Login email page")
        logger.debug(self.browser.current_url)
        email_input = self.__wait_element(xpath_selector='.//input[@id="identifierId"]')
        email_input.send_keys(self.context['account']['email'])
        next_button = self.__wait_element(xpath_selector='.//div[@id="identifierNext"]')
        self.__click_on_element(elem=next_button)

    @document_handler(Priority.Highest,
                      [RuleSet(Rule('accounts.google.com/signin/v2/sl/pwd', PlainText, CaseLess, Mandatory, Url))],
                      invocation_limit=1)
    def login_password_page(self):
        logger.debug("Login email page")
        logger.debug(self.browser.current_url)
        password_input = self.__wait_element(xpath_selector='.//input[@type="password"]')
        password_input.send_keys(self.context['account']['password'])
        next_button = self.__wait_element(xpath_selector='.//div[@id="passwordNext"]')
        self.__click_on_element(elem=next_button)

    @document_handler(Priority.Highest,
                      [RuleSet(
                          Rule('accounts.google.com/signin/v2/challenge/selection', PlainText, CaseLess, Mandatory, Url))],
                      invocation_limit=1)
    def login_confirm_identity_page(self):
        logger.debug("Login confirm identity page")
        logger.debug(self.browser.current_url)
        voice_li = self.__wait_element(xpath_selector='.//div[@data-sendmethod="VOICE"]/..')
        self.__click_on_element(elem=voice_li)

    @document_handler(Priority.Highest,
                      [RuleSet(Rule('accounts.google.com/signin/v2/challenge/ipp', PlainText, CaseLess, Mandatory, Url))],
                      invocation_limit=1)
    def login_phone_page(self):
        logger.debug("Login phone page")
        logger.debug(self.browser.current_url)
        password_input = self.__wait_element(xpath_selector='.//input[@type="tel"]')
        user_input = input('Enter received sms code:')
        password_input.send_keys(user_input)
        next_button = self.__wait_element(xpath_selector='.//div[@id="idvPreregisteredPhoneNext"]')
        self.__click_on_element(elem=next_button)

    @document_handler(Priority.Highest,
                      [RuleSet(
                          Rule('ads.google.com/um/Welcome/Home', PlainText, CaseLess, Mandatory, Url))],
                      invocation_limit=1)
    def welcome_home_page(self):
        logger.debug("Welcome home page")
        logger.debug(self.browser.current_url)

        client_customer_id = self.context['account']['client_customer_id']
        account_span = self.__wait_element(xpath_selector=f'.//span[contains(text(), "{client_customer_id}")]')
        self.__click_on_element(elem=account_span)

    @document_handler(Priority.Highest,
                      [RuleSet(
                          Rule('adwords.google.com/aw/overview', PlainText, CaseLess, Mandatory, Url))],
                      invocation_limit=1)
    def overview_page(self):
        logger.debug("Overview home page")
        logger.debug(self.browser.current_url)

        self.status = WorkflowStatus.WorkflowSuccess

    @document_handler(Priority.Low,
                      [RuleSet(
                          Rule('accounts.google.com/CheckCookie', PlainText, CaseLess, Mandatory, Url))],
                      invocation_limit=5)
    def check_cookie(self):
        logger.debug("Check cookie page")
        time.sleep(5)

    def __wait_element(self, xpath_selector, parent=None, timeout=30):
        if not parent:
            parent = self.browser
        logger.debug(f'Locating {xpath_selector}')
        try:
            WebDriverWait(self.browser, timeout).until(
                EC.visibility_of_element_located(
                    (By.XPATH, xpath_selector))
            )
        except (TimeoutException, NoSuchElementException) as e:
            logger.error(
                f'{xpath_selector} not found. Abort.',
                exc_info=True)
            raise e

        return parent.find_element_by_xpath(xpath_selector)

    def __click_on_element(self, elem, retries=2):
        logger.debug(f'Click on element {elem}')
        try:
            elem.click()
        except Exception as e:
            if retries > 0:
                retries = retries-1
                logger.warning(f'Click failed, retry({retries})')
                time.sleep(3)
                self.__click_on_element(elem=elem, retries=retries)
            else:
                raise e


def kw_planner(account, retries=5):
    while retries > 0:
        profile = webdriver.FirefoxProfile()
        profile.set_preference("print.always_print_silent", True)
        profile.set_preference("print.show_print_progress", False)
        profile.set_preference("browser.link.open_newwindow", 1)
        profile.set_preference('permissions.default.image', 2)
        profile.set_preference('browser.download.manager.showWhenStarting', False)
        profile.set_preference("browser.helperApps.neverAsk.saveToDisk", "application/csv")
        profile.set_preference("browser.helperApps.neverAsk.saveToDisk", "text/plain")
        profile.set_preference("browser.helperApps.neverAsk.saveToDisk", "text/comma-separated-values")
        profile.set_preference("browser.helperApps.neverAsk.openFile", "application/octet-stream")
        profile.set_preference("browser.helperApps.neverAsk.saveToDisk", "application/octet-stream")
        # https://support.mozilla.org/en-US/questions/1167673
        # profile.set_preference("browser.tabs.remote.autostart", False)
        # profile.set_preference("browser.tabs.remote.autostart2", False)
        profile.set_preference('dom.ipc.plugins.enabled.libflashplayer.so', False)

        browser = webdriver.Remote(
            command_executor=f'http://127.0.0.1:4444/wd/hub',
            desired_capabilities=DesiredCapabilities.FIREFOX,
            browser_profile=profile)

        browser.get('https://ads.google.com/intl/en/home/')

        worker = KWPlannerWorkflowHandler(browser, {
            'account': account
        })
        worker.dispatch_while_working()
        try:
            browser.quit()
        except WebDriverException:
            pass
        if worker.status == WorkflowStatus.WorkflowSuccess:
            return worker.status

        logger.warning(f'Exception found. Retry...')
        retries = retries - 1

    return WorkflowStatus.WorkflowFailure


def main():
    parser = argparse.ArgumentParser(description='First time sms identify google adwords account')
    parser.add_argument('--email', required=True, help='Username')
    parser.add_argument('--password', required=True, help='Password')
    parser.add_argument('--client-customer-id', required=True, help='Client customer id')

    args = parser.parse_args()

    account = {
        'email': args.email,
        'password': args.password,
        'client_customer_id': args.client_customer_id
    }

    status = kw_planner(account=account)
    if status == WorkflowStatus.WorkflowSuccess:
        logging.info('Account been registered to use without phone confirmation. '
                     'Now you can add current account info to settings')
        logging.info(account)




if __name__ == "__main__":
    main()