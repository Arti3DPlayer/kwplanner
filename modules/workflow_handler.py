import logging
import inspect
import operator
import traceback
import time

from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException, NoSuchElementException, WebDriverException, SessionNotCreatedException


from .ancillary import regex_search, regex_match, regex_test

logger = logging.getLogger(__name__)


class WorkflowStatus:
    WorkflowWorking = 0
    WorkflowSuccess = 1
    WorkflowFailure = 2


class Priority:
    Highest = 20
    High = 40
    Medium = 60
    Low = 80
    Lowest = 100


class WorkflowError:
    message = ''
    page_source = ''
    exec_info = str()
    screenshot = None

    def __init__(self, message='Internal error', exec_info='', page_source='', screenshot=None):
        self.message = message
        self.exec_info = exec_info
        self.page_source = page_source
        self.screenshot = screenshot


class ContinueEvaluating:
    pass


class StopEvaluating:
    pass


class Mandatory:
    pass


class Optional:
    pass


class PlainText:
    pass


class CaseLess:
    pass


class Inverse:
    pass


class Regex:
    pass


class Body:
    pass


class Url:
    pass


class RuleSet(list):

    def __init__(self, *args, **kwargs):
        for rule in args:
            if not isinstance(rule, Rule):
                raise ValueError(u'Incorrect rule element [%s]' % (str(rule)))

        self.rules = args
        self.modifiers = []
        self.dependencies = []

        if 'modifiers' in kwargs:
            self.modifiers = kwargs['modifiers']

        if 'dependencies' in kwargs:
            self.dependencies = kwargs['dependencies']

        for modifier in self.modifiers:
            if modifier not in [ContinueEvaluating, StopEvaluating]:
                raise ValueError('Invalid ruleset modifier %s' % (str(modifier)))

        if StopEvaluating not in self.modifiers and ContinueEvaluating not in self.modifiers:
            self.modifiers.append(StopEvaluating)


class Rule(object):
    sequence = 0

    def __init__(self, rule_text, *rule_modifiers):
        if type(rule_text) != str:
            raise ValueError(u'Invalid rule text value')

        for rule_modifier in rule_modifiers:
            if rule_modifier not in [Mandatory, Optional, PlainText, CaseLess, Regex, Inverse, Body, Url]:
                raise ValueError(u'Invalid rule modifier %s' % (str(rule_modifier)))

        Rule.sequence += 1

        self.rule_id = Rule.sequence
        self.rule_text = rule_text
        self.rule_modifiers = list(rule_modifiers)

        if Mandatory not in self.rule_modifiers and Optional not in self.rule_modifiers:
            self.rule_modifiers.append(Mandatory)

        if Regex not in self.rule_modifiers and PlainText not in self.rule_modifiers:
            self.rule_modifiers.append(Regex)

        if Url not in self.rule_modifiers and Body not in self.rule_modifiers:
            self.rule_modifiers.append(Body)


class document_handler(object):
    def __init__(self, priority=None, rulesets=None, invocation_limit=None, modifiers=None, dependencies=None):
        self.rulesets = rulesets or []
        self.priority = priority or Priority.Lowest
        self.modifiers = modifiers or []
        self.dependencies = dependencies or []
        self.invocation_limit = invocation_limit or 999999

        for modifier in self.modifiers:
            if modifier not in [ContinueEvaluating, StopEvaluating]:
                raise ValueError(u'Invalid ruleset modifier %s' % (str(modifier)))

    def __call__(self, f):
        f.rulesets = self.rulesets
        f.priority = self.priority
        f.modifiers = self.modifiers
        f.dependencies = self.dependencies
        f.invocation_count = 0
        f.invocation_limit = self.invocation_limit

        return f


class WorkflowHandler(object):
    def __init__(self, browser, context):
        self.status = WorkflowStatus.WorkflowWorking
        self.status_ = {}
        self.browser = browser
        self.context = context
        self.context['errors'] = self.context.get('errors', [])
        self.handlers = []
        self.scoreboard = {}

        for method in inspect.getmembers(self, predicate=inspect.ismethod):
            if 'priority' in map(lambda m: m[0], inspect.getmembers(method[1])):
                handler = method[1]

                self.handlers.append(handler)
                self.scoreboard[handler.__func__.__name__] = 0

        self.handlers = sorted(self.handlers, key=operator.attrgetter('priority'))

    def set_dependency_status(self, dependency_name, dependency_status):
        if dependency_status:
            self.status_[dependency_name] = True
        else:
            del self.status_[dependency_name]

    def get_dependency_status(self, dependency_name):
        return self.status_.get(dependency_name.lstrip('!')) or False

    def dispatch_while_working(self, *args, **kwargs):
        try:
            while self.status == WorkflowStatus.WorkflowWorking:
                self.dispatch(*args, **kwargs)
                time.sleep(5)
        except Exception as e:
            self.status = WorkflowStatus.WorkflowFailure
            logger.error('There was an exception on the page handler', exc_info=True)

            workflow_error = WorkflowError()

            workflow_error.exec_info = traceback.format_exc()

            try:
                workflow_error.screenshot = self.__screenshot_full_page_as_base64()
            except Exception:
                logger.error('Can\'t make screenshot of html element', exc_info=True)

            try:
                elem = self.browser.find_element(By.XPATH, '//*')
                workflow_error.page_source = elem.get_attribute('outerHTML')
            except Exception:
                logger.error('Can\'t get page source', exc_info=True)

            self.context['errors'].append(workflow_error)

    def dispatch(self, *args, **kwargs):
        results = []
        ruleset_matches = 0

        for handler in self.handlers:

            if self.scoreboard[handler.__func__.__name__] < handler.invocation_limit:
                dependency_matches = 0

                for dependency in handler.dependencies:
                    dependency_status = self.get_dependency_status(dependency)

                    if dependency.startswith('!'):
                        dependency_status = not dependency_status

                    if dependency_status:
                        dependency_matches += 1

                if dependency_matches == len(handler.dependencies):
                    ruleset_matched = False
                    terminator_ruleset = False

                    if not handler.rulesets:
                        ruleset_matched = True

                    else:
                        for ruleset in handler.rulesets:

                            if ruleset_matched:
                                break

                            dependency_matches = 0

                            for dependency in ruleset.dependencies:
                                dependency_status = self.get_dependency_status(dependency)

                                if dependency.startswith('!'):
                                    dependency_status = not dependency_status

                                if dependency_status:
                                    dependency_matches += 1

                            if dependency_matches == len(ruleset.dependencies):
                                ruleset_result = True

                                for rule in ruleset.rules:
                                    rule_result = None

                                    rule_text = rule.rule_text
                                    # because of marionette driver error
                                    # we don't get page source or url for every page if it don't have
                                    # this rule
                                    try:
                                        if Body in rule.rule_modifiers:
                                            body_text = self.browser.page_source
                                        else:
                                            body_text = ''

                                        if Url in rule.rule_modifiers:
                                            url_text = self.browser.current_url
                                        else:
                                            url_text = ''
                                    except (WebDriverException, KeyError) as e:
                                        logger.warning('Cant get page source')
                                        retries = kwargs.get('retries', 5)
                                        retries = retries - 1
                                        if retries > 0:
                                            time.sleep(5)
                                            self.dispatch(self, retries=retries, *args, **kwargs)
                                            return
                                        raise e

                                    if CaseLess in rule.rule_modifiers:
                                        rule_text = rule_text.lower()
                                        body_text = body_text.lower()
                                        url_text = url_text.lower()

                                    if Body in rule.rule_modifiers:
                                        if Regex in rule.rule_modifiers:
                                            rule_result = regex_test(rule_text, body_text)

                                        elif PlainText in rule.rule_modifiers:
                                            rule_result = rule_text in body_text

                                    elif Url in rule.rule_modifiers:

                                        if Regex in rule.rule_modifiers:

                                            rule_result = regex_test(rule_text, url_text)

                                        elif PlainText in rule.rule_modifiers:
                                            rule_result = rule_text in url_text

                                    if Inverse in rule.rule_modifiers:
                                        rule_result = not rule_result

                                    if Mandatory in rule.rule_modifiers and not rule_result:
                                        ruleset_result = False
                                        break

                                if ruleset_result:
                                    ruleset_matches += 1
                                    ruleset_matched = True

                                    if StopEvaluating in ruleset.modifiers:
                                        terminator_ruleset = True

                    if ruleset_matched:
                        results.append(handler(*args, **kwargs))

                        self.scoreboard[handler.__func__.__name__] += 1

                        if StopEvaluating in handler.modifiers or terminator_ruleset:
                            return

        if not ruleset_matches:
            self.status = WorkflowStatus.WorkflowFailure
            logger.error(f'Handler not found for page {self.browser.current_url}')

            workflow_error = WorkflowError()

            workflow_error.exec_info = f'Handler not found for page {self.browser.current_url}'
            try:
                workflow_error.screenshot = self.__screenshot_full_page_as_base64()
            except Exception:
                logger.error('Can\'t make screenshot of html element', exc_info=True)

            try:
                elem = self.browser.find_element(By.XPATH, '//*')
                workflow_error.page_source = elem.get_attribute('outerHTML')
            except Exception:
                logger.error('Can\'t get page source', exc_info=True)

            self.context['errors'].append(workflow_error)

    def __screenshot_full_page_as_base64(self):
        element = self.browser.find_elements_by_xpath("/html/child::*/child::*")
        eheight = set()
        for e in element:
            eheight.add(round(e.size["height"]))
        total_height = sum(eheight)
        self.browser.execute_script(
            "document.getElementsByTagName('html')[0].setAttribute('style', 'height:" + str(total_height) + "px')")
        element = self.browser.find_element_by_tag_name('html')
        return element.screenshot_as_base64
