
class ProxyNotFoundException(Exception):
    """
       Raises when working proxy not been found
    """
    pass
