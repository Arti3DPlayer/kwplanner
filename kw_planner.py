import os
import time
import pickle
import random
import argparse
import shutil
import logging
import logging.config
import glob
import csv
import operator

import settings

from datetime import datetime
from urllib.parse import urlencode
import multiprocessing

from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities

from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait, Select
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException, NoSuchElementException,\
    WebDriverException, UnexpectedAlertPresentException, StaleElementReferenceException

from modules.workflow_handler import WorkflowHandler, WorkflowStatus, RuleSet, Rule, Priority,\
    PlainText, CaseLess, Mandatory, Url, Regex, document_handler, WorkflowError

from watchdog import Watchdog, WatchdogException
from utils import chunks, yes_or_no

LOGS_DIR = "logs"
DOWNLOADS_DIR = "downloads"
TEMPORARY_DIR = "tmp"

if not os.path.exists(os.path.join(settings.ROOT_PATH, LOGS_DIR)):
    os.makedirs(os.path.join(settings.ROOT_PATH, LOGS_DIR))

if not os.path.exists(os.path.join(settings.ROOT_PATH, DOWNLOADS_DIR)):
    os.makedirs(os.path.join(settings.ROOT_PATH, DOWNLOADS_DIR))

if not os.path.exists(os.path.join(settings.ROOT_PATH, TEMPORARY_DIR)):
    os.makedirs(os.path.join(settings.ROOT_PATH, TEMPORARY_DIR))

logging.config.fileConfig('logging.conf')
logger = logging.getLogger(__name__)


class KWPlannerWorkflowHandler(WorkflowHandler):

    @document_handler(Priority.Highest,
                      [RuleSet(Rule('ads.google.com/intl/en/home/', PlainText, CaseLess, Mandatory, Url))],
                      invocation_limit=1)
    def home_page(self):
        logger.debug("Home page")
        logger.debug(self.browser.current_url)
        login_link = self.__wait_element(xpath_selector='.//a[@class="cta-signin"]')
        self.__click_on_element(elem=login_link)

    @document_handler(Priority.Highest,
                      [RuleSet(Rule('accounts.google.com/signin/v2/identifier', PlainText, CaseLess, Mandatory, Url))],
                      invocation_limit=1)
    def login_email_page(self):
        logger.debug("Login email page")
        logger.debug(self.browser.current_url)
        email_input = self.__wait_element(xpath_selector='.//input[@id="identifierId"]')
        email_input.send_keys(self.context['account']['email'])
        next_button = self.__wait_element(xpath_selector='.//div[@id="identifierNext"]')
        self.__click_on_element(elem=next_button)

    @document_handler(Priority.Highest,
                      [RuleSet(Rule('accounts.google.com/signin/v2/sl/pwd', PlainText, CaseLess, Mandatory, Url))],
                      invocation_limit=1)
    def login_password_page(self):
        logger.debug("Login email page")
        logger.debug(self.browser.current_url)
        password_input = self.__wait_element(xpath_selector='.//input[@type="password"]')
        password_input.send_keys(self.context['account']['password'])
        next_button = self.__wait_element(xpath_selector='.//div[@id="passwordNext"]')
        self.__click_on_element(elem=next_button)

    @document_handler(Priority.Highest,
                      [RuleSet(
                          Rule('accounts.google.com/signin/v2/challenge/selection', PlainText, CaseLess, Mandatory, Url))],
                      invocation_limit=1)
    def login_confirm_identity_page(self):
        logger.debug("Login confirm identity page")
        logger.debug(self.browser.current_url)
        voice_li = self.__wait_element(xpath_selector='.//div[@data-sendmethod="VOICE"]/..')
        self.__click_on_element(elem=voice_li)

    @document_handler(Priority.Highest,
                      [RuleSet(Rule('accounts.google.com/signin/v2/challenge/ipp', PlainText, CaseLess, Mandatory, Url))],
                      invocation_limit=1)
    def login_phone_page(self):
        logger.debug("Login phone page")
        logger.debug(self.browser.current_url)
        password_input = self.__wait_element(xpath_selector='.//input[@type="tel"]')
        user_input = input('Enter received sms code:')
        password_input.send_keys(user_input)
        next_button = self.__wait_element(xpath_selector='.//div[@id="idvPreregisteredPhoneNext"]')
        self.__click_on_element(elem=next_button)

    @document_handler(Priority.Highest,
                      [RuleSet(
                          Rule('ads.google.com/um/Welcome/Home', PlainText, CaseLess, Mandatory, Url))],
                      invocation_limit=1)
    def welcome_home_page(self):
        logger.debug("Welcome home page")
        logger.debug(self.browser.current_url)

        client_customer_id = self.context['account']['client_customer_id']
        account_span = self.__wait_element(xpath_selector=f'.//span[contains(text(), "{client_customer_id}")]')
        self.__click_on_element(elem=account_span)

    @document_handler(Priority.Highest,
                      [RuleSet(
                          Rule('ads.google.com/aw/overview', PlainText, CaseLess, Mandatory, Url))],
                      invocation_limit=1)
    def overview_page(self):
        logger.debug("Overview home page")
        logger.debug(self.browser.current_url)

        tools_button = self.__wait_element(xpath_selector='.//material-button[@navi-id="app-bar-mega-menu-button"]')
        new_url = self.browser.current_url.replace('overview', 'keywordplanner/home')
        self.browser.get(new_url)

    @document_handler(Priority.Highest,
                      [RuleSet(
                          Rule('ads.google.com/aw/keywordplanner/home', PlainText, CaseLess, Mandatory, Url))],
                      invocation_limit=1)
    def keyword_planner_home_page(self):
        logger.debug("Keyword planner home page")
        logger.debug(self.browser.current_url)

        # select account in popup if appear
        try:
            account_row = self.__wait_element(xpath_selector='.//span[@class="picker-customer-id"]/../../../..')
            self.__click_on_element(elem=account_row)
            time.sleep(5)
        except WebDriverException:
            pass

        gs_block = self.__wait_element(xpath_selector='.//span[contains(text(), "Get search volume and forecasts")]')
        self.__click_on_element(elem=gs_block)
        time.sleep(5)
        keywords_textarea = self.__wait_element(xpath_selector='.//textarea')
        # Sending a big string via send_keys take a lot of performance and time
        # keywords_textarea.send_keys(self.context['keywords'])
        # so we fill textarea via js and then to generate onchange event for angular site send some string
        self.browser.execute_script("arguments[0].value = arguments[1]", keywords_textarea, self.context['keywords'])
        time.sleep(5)
        keywords_textarea.send_keys(', ')
        time.sleep(3)
        get_started_btn = self.__wait_element(xpath_selector='.//material-button[contains(@class, "get-results-button")]')
        self.__click_on_element(elem=get_started_btn)

    @document_handler(Priority.Highest,
                      [RuleSet(
                          Rule('ads.google.com/aw/keywordplanner/plan/keywords/forecasts', PlainText, CaseLess, Mandatory, Url))],
                      invocation_limit=1)
    def keyword_planner_forecasts_page(self):
        logger.debug("Keyword planner forecasts page")
        logger.debug(self.browser.current_url)

        historical_metrics_tab = self.__wait_element(
            xpath_selector='.//tab-button/div[contains(text(), "HISTORICAL METRICS")]/..')
        self.__click_on_element(elem=historical_metrics_tab)

    @document_handler(Priority.Highest,
                      [RuleSet(
                          Rule('ads.google.com/aw/keywordplanner/plan/keywords/historical', PlainText, CaseLess,
                               Mandatory, Url))],
                      invocation_limit=1)
    def keyword_planner_historical_page(self):
        logger.debug("Keyword planner historical page")
        logger.debug(self.browser.current_url)

        download_keywords_btn = self.__wait_element(xpath_selector='.//material-button-dropdown[contains(@class, "download")]')
        self.__click_on_element(elem=download_keywords_btn)
        time.sleep(1)
        historical_item = self.__wait_element(xpath_selector='.//material-list[contains(@class, "download-menu")]//material-list-item[contains(@class, "download-plan-stats")]')
        self.__click_on_element(elem=historical_item)

        try:
            watchdog = Watchdog(30)
            logger.info("Preparing report")
            try:
                WebDriverWait(self.browser, 5).until(
                    EC.visibility_of_element_located(
                        (By.XPATH, './/div[contains(text(), "Preparing report")]'))
                )
                overlay_el = self.browser.find_element_by_xpath('.//div[contains(text(), "Preparing report")]')
                while overlay_el.is_displayed():
                    time.sleep(0.2)
            except (StaleElementReferenceException, TimeoutException, NoSuchElementException):
                pass

            logger.debug("Downloading report")
            try:
                WebDriverWait(self.browser, 5).until(
                    EC.visibility_of_element_located(
                        (By.XPATH, './/div[contains(text(), "Downloading report")]'))
                )
                overlay_el = self.browser.find_element_by_xpath('.//div[contains(text(), "Downloading report")]')
                while overlay_el.is_displayed():
                    time.sleep(0.2)
            except (StaleElementReferenceException, TimeoutException, NoSuchElementException):
                pass
            watchdog.stop()
            logger.info("Finishing")
            time.sleep(5)
            self.status = WorkflowStatus.WorkflowSuccess
        except WatchdogException:
            logger.warning('File not been generated.')
            self.status = WorkflowStatus.WorkflowFailure

    @document_handler(Priority.Low,
                      [RuleSet(
                          Rule('ads.google.com/CheckCookie', PlainText, CaseLess, Mandatory, Url))],
                      invocation_limit=5)
    def check_cookie(self):
        logger.debug("Check cookie page")
        time.sleep(5)

    def __wait_element(self, xpath_selector, parent=None, timeout=60):
        if not parent:
            parent = self.browser
        logger.debug(f'Locating {xpath_selector}')
        try:
            WebDriverWait(self.browser, timeout).until(
                EC.visibility_of_element_located(
                    (By.XPATH, xpath_selector))
            )
        except (TimeoutException, NoSuchElementException) as e:
            logger.error(
                f'{xpath_selector} not found. Abort.',
                exc_info=True)
            raise e

        return parent.find_element_by_xpath(xpath_selector)

    def __click_on_element(self, elem, retries=2):
        logger.debug(f'Click on element {elem}')
        try:
            elem.click()
        except Exception as e:
            if retries > 0:
                retries = retries-1
                logger.warning(f'Click failed, retry({retries})')
                time.sleep(3)
                self.__click_on_element(elem=elem, retries=retries)
            else:
                raise e


def read_keywords(file_path):
    logger.info('Reading keywords')
    keywords = []
    with open(file_path, encoding='latin-1') as f:
        for line in f:
            keywords.append(line.strip())
    logger.info(f'Read {len(keywords)} keywords')
    keywords_chunks = list(chunks(keywords, settings.KW_PLANNER_CHUNK_SIZE))
    logger.info(f'Divided keywords into {len(keywords_chunks)} chunks')
    return keywords_chunks


def clear_downloads_folder():
    logger.debug('Clearing downloads folder')
    folder = os.path.join(settings.ROOT_PATH, 'downloads')

    for the_file in os.listdir(folder):
        file_path = os.path.join(folder, the_file)
        try:
            if os.path.isfile(file_path):
                os.unlink(file_path)
                # elif os.path.isdir(file_path): shutil.rmtree(file_path)
        except Exception as e:
            print(e)


def concat_downloaded_csv_files(output_path):
    folder = os.path.join(settings.ROOT_PATH, DOWNLOADS_DIR)
    header = []
    data = []
    try:
        if os.path.isfile(output_path):
            os.unlink(output_path)
    except Exception as e:
        print(e)

    with open(output_path, 'w', encoding='utf-16') as singleFile:
        for csvFile in glob.glob(os.path.join(folder, '*.csv')):
            f = open(csvFile, 'r', encoding='utf-16')
            # skip the header
            f.__next__()
            f.__next__()
            header = f.readline().split('\t')
            f.__next__()
            f.__next__()

            for line in f:
                s_line = line.split('\t')
                if len(s_line) == len(header):
                    s_line[4] = s_line[4].replace('\"', '').replace(',', '')
                    s_line[5] = s_line[5].replace('\"', '').replace(',', '')
                    data.append([s_line[0], s_line[4], s_line[5]])

        writer = csv.writer(singleFile)
        data.insert(0, ['Keyword', 'Min search volume', 'Max search volume'])
        writer.writerows(data)


def order_result_csv(output_path):
    from itertools import groupby
    with open(output_path, 'r', encoding='utf-16') as f_input:
        csv_input = csv.DictReader(f_input)
        data = sorted(csv_input, key=lambda row: (int(row['Min search volume']),
                                                  int(row['Max search volume'])), reverse=True)
    try:
        if os.path.isfile(output_path):
            os.unlink(output_path)
    except Exception as e:
        print(e)

    with open(output_path, 'w', encoding='utf-16') as f_output:
        csv_output = csv.DictWriter(f_output, fieldnames=csv_input.fieldnames)
        csv_output.writeheader()
        for group_keyword, keywords in groupby(data, key=lambda row: row['Keyword'].split(' ')[0]):
            csv_output.writerow({'Keyword': group_keyword})
            for keyword in keywords:
                csv_output.writerow(keyword)
            csv_output.writerow({})


def kw_planner(keywords, account, retries=5):
    while retries > 0:
        profile = webdriver.FirefoxProfile()
        profile.set_preference("print.always_print_silent", True)
        profile.set_preference("print.show_print_progress", False)
        profile.set_preference("browser.link.open_newwindow", 1)
        profile.set_preference('permissions.default.image', 2)
        profile.set_preference('browser.download.manager.showWhenStarting', False)
        profile.set_preference("browser.helperApps.neverAsk.saveToDisk", "application/csv")
        profile.set_preference("browser.helperApps.neverAsk.saveToDisk", "text/plain")
        profile.set_preference("browser.helperApps.neverAsk.saveToDisk", "text/comma-separated-values")
        profile.set_preference("browser.helperApps.neverAsk.openFile", "application/octet-stream")
        profile.set_preference("browser.helperApps.neverAsk.saveToDisk", "application/octet-stream")
        # https://support.mozilla.org/en-US/questions/1167673
        # profile.set_preference("browser.tabs.remote.autostart", False)
        # profile.set_preference("browser.tabs.remote.autostart2", False)
        profile.set_preference('dom.ipc.plugins.enabled.libflashplayer.so', False)

        browser = webdriver.Remote(
            command_executor=f'http://127.0.0.1:4444/wd/hub',
            desired_capabilities=DesiredCapabilities.FIREFOX,
            browser_profile=profile)

        browser.get('https://ads.google.com/intl/en/home/')

        worker = KWPlannerWorkflowHandler(browser, {
            'keywords': ', '.join(keywords),
            'account': account
        })
        worker.dispatch_while_working()
        try:
            browser.quit()
        except WebDriverException:
            pass
        if worker.status == WorkflowStatus.WorkflowSuccess:
            return worker.status

        logger.warning(f'Exception found. Retry...')
        retries = retries - 1

    return WorkflowStatus.WorkflowFailure


def run_kw_planners(keywords):
    process_name = multiprocessing.current_process().name
    logger.info(f'Process {process_name}')
    for index, keywords_chunk in enumerate(keywords):
        logger.info(f'Current progress: {index}/{len(keywords)}')
        logger.info(f'Portion: {index*settings.KW_PLANNER_CHUNK_SIZE} - '
                    f'{index*settings.KW_PLANNER_CHUNK_SIZE+len(keywords_chunk)}')

        account = random.choice(settings.ADWORDS_ACCOUNTS)
        status = kw_planner(account=account, keywords=keywords_chunk)

        if status == WorkflowStatus.WorkflowFailure:
            logging.error('Out of retries')


def main():
    parser = argparse.ArgumentParser(description='Automation google keywords planner.')
    parser.add_argument('--keywords-file-path', required=True, help='Path to file with keywords')
    parser.add_argument('--output-path', required=True, help='Path to output file')

    args = parser.parse_args()

    if yes_or_no('Do you want to erase old downloads data?'):
        clear_downloads_folder()
    keywords = read_keywords(args.keywords_file_path)

    pool = multiprocessing.Pool()

    # max number of parallel process
    iteration_count = settings.NUMBER_OF_PROCESSES

    count_per_iteration = len(keywords) / float(iteration_count)
    results = []
    for i in range(0, iteration_count):
        list_start = int(count_per_iteration * i)
        list_end = int(count_per_iteration * (i + 1))

        p = pool.apply_async(run_kw_planners, [keywords[list_start:list_end]])
        results.append(p)

    [p.get() for p in results]

    concat_downloaded_csv_files(args.output_path)
    order_result_csv(args.output_path)


if __name__ == "__main__":
    main()