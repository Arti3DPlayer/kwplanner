import os
import time
import pickle
import random
import argparse
import shutil
import logging
import logging.config
import glob
import csv
import operator

import settings

from datetime import datetime
from urllib.parse import urlencode, urlparse
import multiprocessing

from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities

from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait, Select
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException, NoSuchElementException,\
    WebDriverException, UnexpectedAlertPresentException, StaleElementReferenceException

from modules.workflow_handler import WorkflowHandler, WorkflowStatus, RuleSet, Rule, Priority,\
    PlainText, CaseLess, Mandatory, Url, Regex, document_handler, WorkflowError

from watchdog import Watchdog, WatchdogException

logging.config.fileConfig('logging.conf')
logger = logging.getLogger(__name__)


class GoogleSearchWorkflowHandler(WorkflowHandler):

    @document_handler(Priority.Highest,
                      [RuleSet(Rule('www.google.com/search', PlainText, CaseLess, Mandatory, Url))],
                      invocation_limit=1)
    def search_page(self):
        logger.debug("Search page")
        logger.debug(self.browser.current_url)

        self.context['rank'] = 'Not found'

        try:
            rank = 1
            WebDriverWait(self.browser, 30).until(
                EC.visibility_of_element_located(
                    (By.XPATH, './/div[@id="search"]'))
            )
            links = self.browser.find_elements_by_xpath(
                "//h3[@class='r']/a[@href]")
            for link in links:
                href = link.get_attribute('href')
                if href != '#':
                    parsed_uri = urlparse(href)
                    if parsed_uri.netloc == self.context['match_url']:
                        self.context['rank'] = rank
                        break
                    rank += 1
            self.status = WorkflowStatus.WorkflowSuccess
        except (StaleElementReferenceException, TimeoutException, NoSuchElementException) as e:
            print(e)
            self.status = WorkflowStatus.WorkflowFailure



    def __wait_element(self, xpath_selector, parent=None, timeout=30):
        if not parent:
            parent = self.browser
        logger.debug(f'Locating {xpath_selector}')
        try:
            WebDriverWait(self.browser, timeout).until(
                EC.visibility_of_element_located(
                    (By.XPATH, xpath_selector))
            )
        except (TimeoutException, NoSuchElementException) as e:
            logger.error(
                f'{xpath_selector} not found. Abort.',
                exc_info=True)
            raise e

        return parent.find_element_by_xpath(xpath_selector)


def google_search(keyword, match_url, number_results, language_code, retries=5):
    while retries > 0:
        profile = webdriver.FirefoxProfile()
        profile.set_preference("print.always_print_silent", True)
        profile.set_preference("print.show_print_progress", False)
        profile.set_preference("browser.link.open_newwindow", 1)
        profile.set_preference('permissions.default.image', 2)
        profile.set_preference('browser.download.manager.showWhenStarting', False)
        profile.set_preference("browser.helperApps.neverAsk.saveToDisk", "application/csv")
        profile.set_preference("browser.helperApps.neverAsk.saveToDisk", "text/plain")
        profile.set_preference("browser.helperApps.neverAsk.saveToDisk", "text/comma-separated-values")
        profile.set_preference("browser.helperApps.neverAsk.openFile", "application/octet-stream")
        profile.set_preference("browser.helperApps.neverAsk.saveToDisk", "application/octet-stream")
        # https://support.mozilla.org/en-US/questions/1167673
        # profile.set_preference("browser.tabs.remote.autostart", False)
        # profile.set_preference("browser.tabs.remote.autostart2", False)
        profile.set_preference('dom.ipc.plugins.enabled.libflashplayer.so', False)

        browser = webdriver.Remote(
            command_executor=f'http://127.0.0.1:4444/wd/hub',
            desired_capabilities=DesiredCapabilities.FIREFOX,
            browser_profile=profile)

        escaped_search_term = keyword.replace(' ', '+')
        google_url = 'https://www.google.com/search?q={}&num={}&hl={}'.format(escaped_search_term, number_results,
                                                                              language_code)

        browser.get(google_url)

        worker = GoogleSearchWorkflowHandler(browser, {
            'keyword': keyword,
            'match_url': match_url
        })
        worker.dispatch_while_working()
        try:
            browser.quit()
        except WebDriverException:
            pass
        if worker.status == WorkflowStatus.WorkflowSuccess:
            return worker.status, worker.context.get('rank')

        logger.warning(f'Exception found. Retry...')
        retries = retries - 1

    return WorkflowStatus.WorkflowFailure, 'Not found'


def run_google_scrappers(keywords, match_url):
    process_name = multiprocessing.current_process().name
    logger.info(f'Process {process_name}')
    data = []
    for index, keyword in enumerate(keywords):
        logger.info(f'Current progress: {index}/{len(keywords)}')
        status, rank = google_search(keyword=keyword, match_url=match_url, number_results=100, language_code='en')

        if status == WorkflowStatus.WorkflowFailure:
            logging.error('Out of retries')
        else:
            d = {'keyword': keyword, 'rank': rank}
            logging.info(d)
            data.append(d)

    return data


def generate_csv_file(output_path, data):

    with open(output_path, 'w', encoding='utf-16') as f:
        fieldnames = ['keyword', 'rank']
        writer = csv.DictWriter(f, fieldnames=fieldnames)
        writer.writerow({'keyword': 'Keyword', 'rank': 'Rating'})
        writer.writerows(data)


def read_keywords(file_path):
    logger.info('Reading keywords')
    keywords = []
    with open(file_path, encoding='latin-1') as f:
        for line in f:
            keywords.append(line.strip())
    logger.info(f'Read {len(keywords)} keywords')
    return keywords


def main():
    parser = argparse.ArgumentParser(description='Google search rating')
    parser.add_argument('--keywords-file-path', required=True, help='Path to file with keywords')
    parser.add_argument('--output-path', required=True, help='Path to output file')

    args = parser.parse_args()

    match_url = 'www.sywebs.nl'#input('Enter url: ')
    keywords = read_keywords(args.keywords_file_path)

    pool = multiprocessing.Pool()

    # max number of parallel process
    iteration_count = settings.NUMBER_OF_PROCESSES

    count_per_iteration = len(keywords) / float(iteration_count)
    results = []
    for i in range(0, iteration_count):
        list_start = int(count_per_iteration * i)
        list_end = int(count_per_iteration * (i + 1))

        p = pool.apply_async(run_google_scrappers, [keywords[list_start:list_end], match_url])
        results.append(p)

    data = sum([p.get() for p in results], [])
    generate_csv_file(args.output_path, data)


if __name__ == "__main__":
    main()